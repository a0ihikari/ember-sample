import Ember from 'ember';

export default Ember.Component.extend({
	isImageShowing: false,
	buttonName: "Show Image",
	ImageIsShowing: function(){
		return this.get("isImageShowing") === false;
	}.property("isImageShowing"),
	actions: {
		imageShow(){
			if (this.get("ImageIsShowing")){
				this.set("isImageShowing", true);
				this.set("buttonName", "Hide Image");
			}else{
				this.set("isImageShowing", false);
				this.set("buttonName", "Show Image");
			}			
		}
	}
});
